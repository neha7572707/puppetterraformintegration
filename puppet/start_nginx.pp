# puppet/manifests/start_nginx.pp

service { 'nginx':
  ensure => running,
  enable => true,
}
