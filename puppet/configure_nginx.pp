# puppet/manifests/configure_nginx.pp

file { '/etc/nginx/nginx.conf':
  ensure  => present,
  content => template('nginx/nginx.conf.erb'),
}
